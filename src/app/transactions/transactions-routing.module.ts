import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TransactionsComponent } from './transactions.component';
import { TransactionsListComponent } from './features/transactions-list/transactions-list.component';
import { TransactionsCreateEditComponent } from './features/transactions-create-edit/transactions-create-edit.component';
import { CanDeactivateService } from '../core/contracts/can-deactivate.service';

const routes: Routes = [
  {
    path: '',
    component: TransactionsComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: TransactionsListComponent },
      { path: 'create', component: TransactionsCreateEditComponent, canDeactivate: [CanDeactivateService] },
      { path: 'edit/:user/:id', component: TransactionsCreateEditComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionsRoutingModule { }
