import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Transaction } from '../models/transaction.model';
import { BrowserDataService } from '../../core/services/browser-data.service';

const apiUrl = `${environment.apiUrl}/txns`;

@Injectable({ providedIn: 'root' })
export class TransactionService {

    constructor(private http: HttpClient, private browserService: BrowserDataService) {

    }

    getAllTransaction() {
    }

    getTransactionOfUser(user): Observable<any> {
        return this.http.get(`${apiUrl}/${this.browserService.getCurrentUser()}`);
    }
    getTransactionById(user, id): Observable<Transaction> {
        return this.http.get<Transaction>(`${apiUrl}/${this.browserService.getCurrentUser()}/${id}`);
    }

    updateTransaction(trx: Transaction) {
        return this.http.post(`${apiUrl}/${this.browserService.getCurrentUser()}/${trx.id}`, trx);
    }

    createTransaction(trx: Transaction) {
        return this.http.post(`${apiUrl}/${this.browserService.getCurrentUser()}`, trx);
    }

    deleteTransaction(trx: Transaction) {
        return this.http.delete(`${apiUrl}/${this.browserService.getCurrentUser()}/${trx.id}`);
    }
}

