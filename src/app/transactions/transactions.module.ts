import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransactionsRoutingModule } from './transactions-routing.module';
import { TransactionsComponent } from './transactions.component';
import { TransactionsListComponent } from './features/transactions-list/transactions-list.component';
import { MaterialModule } from '../material.module';
import { TableModule } from 'primeng/table';
import { TransactionsCreateEditComponent } from './features/transactions-create-edit/transactions-create-edit.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CoreModule } from '../core/core.module';

@NgModule({
  imports: [
    CommonModule,
    TransactionsRoutingModule,
    MaterialModule,
    TableModule,
    ReactiveFormsModule,
    FormsModule,
    CoreModule,
  ],
  declarations: [
    TransactionsComponent,
    TransactionsListComponent,
    TransactionsCreateEditComponent
  ]
})
export class TransactionsModule { }
