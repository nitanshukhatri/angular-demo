import { Component, OnInit, ViewChild } from '@angular/core';
import { TransactionService } from '../../services/transactions.service';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { BrowserDataService } from '../../../core/services/browser-data.service';
import { SpinnerService } from 'src/app/core/components/spinner/spinner.service';
import { ToasterService } from '../../../core/services/toaster.service';
import { Transaction } from '../../models/transaction.model';
import { DialogService } from 'src/app/core/services/dialog.service';


@Component({
  selector: 'app-transactions-list',
  templateUrl: './transactions-list.component.html',
  styleUrls: ['./transactions-list.component.css']
})
export class TransactionsListComponent implements OnInit {
  user = 'priya@gmail.com';
  transactionList = [];
  displayedColumns: string[] = ['id', 'user', 'amount', 'currency', 'txn_date', 'actions'];
  // tableColumnHeaders = [
  //   { key: 'id', title: 'Id' },
  //   { key: 'user', title: 'User' },
  //   { key: 'amount', title: 'Amount' },
  //   { key: 'currency', title: 'Currency' },
  //   { key: 'txn_date', title: 'Date' }
  // ];
  dataSource: any;
  searchText: string;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private router: Router,
    private transactionService: TransactionService,
    private browserService: BrowserDataService,
    private spinnerService: SpinnerService,
    private toasterService: ToasterService,
    private dialogService: DialogService) { }

  ngOnInit() {
    this.browserService.setCurrentUser(this.user);
    this.fetchtransactions();
  }

  addNewTransaction() {
  }

  fetchtransactions() {
    this.spinnerService.show();
    this.transactionService.getTransactionOfUser(this.user).subscribe((res) => {
      this.spinnerService.hide();
      this.transactionList = res;
      this.dataSource = new MatTableDataSource<any>(this.transactionList);
      this.dataSource.paginator = this.paginator;
    }, err => {
      this.spinnerService.hide();
      this.toasterService.showToaster('Something went Wrong');
    });
  }

  editTransaction(user, id) {
    console.log(id);
    this.router.navigate(['/transactions/edit/', user, id]);
  }

  deleteTransaction(trx: Transaction) {
    this.dialogService.openDialog('Do you want to delete this record ?').afterClosed().subscribe(val => {
      if (val) {
        this.transactionService.deleteTransaction(trx).subscribe(res => {
          const findTrx = this.transactionList.findIndex(tx => tx.id === trx.id);
          this.transactionList.splice(findTrx, 1);
          this.dataSource = new MatTableDataSource<any>(this.transactionList);
          this.toasterService.showToaster('Deleted Successfully');
        }, err => {
          this.toasterService.showToaster('Something Went Wrong');
        });
      }

    });

  }

  filterList() {
    let filterValue = this.searchText;
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
