import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionsCreateEditComponent } from './transactions-create-edit.component';

describe('TransactionsCreateEditComponent', () => {
  let component: TransactionsCreateEditComponent;
  let fixture: ComponentFixture<TransactionsCreateEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionsCreateEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionsCreateEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
