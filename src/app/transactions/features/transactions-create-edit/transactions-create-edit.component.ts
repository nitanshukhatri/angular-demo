import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { TransactionService } from '../../services/transactions.service';
import { ToasterService } from '../../../core/services/toaster.service';
import { BrowserDataService } from '../../../core/services/browser-data.service';
import { SpinnerService } from 'src/app/core/components/spinner/spinner.service';
import { DatePipe } from '@angular/common';
import { distinctUntilChanged } from 'rxjs/operators';
import { Validate } from '../../../core/models/validate';

@Component({
  selector: 'app-transactions-create-edit',
  templateUrl: './transactions-create-edit.component.html',
  styleUrls: ['./transactions-create-edit.component.css'],
  providers: [DatePipe]
})
export class TransactionsCreateEditComponent implements OnInit {

  constructor(private router: Router,
    private currentRoute: ActivatedRoute,
    private transactionService: TransactionService,
    private toasterService: ToasterService,
    private browserService: BrowserDataService,
    private spinnerService: SpinnerService,
    private datePipe: DatePipe) { }

  transactionForm: FormGroup;
  currentPath: String;
  currentUser: String = this.browserService.getCurrentUser();
  @ViewChild('transactionNgForm') form;

  ngOnInit() {

    this.transactionForm = new FormGroup({
      user: new FormControl(this.currentUser, [Validators.required, Validators.email]),
      amount: new FormControl('', [Validators.required, Validate.isMin(3)]),
      currency: new FormControl('', [Validators.required]),
      txn_date: new FormControl('', [Validators.required])
    });

    this.currentPath = this.currentRoute.snapshot.url[0].path;
    this.transactionForm.controls['user'].disable();
    if (this.currentPath === 'edit') {
      this.fetchTransaction();
    }

    // this.transactionForm.get('amount').valueChanges
    //   .pipe(distinctUntilChanged())
    //   .subscribe((mode: string) => {
    //     this.transactionForm.get('amount').setValidators([
    //       Validators.required,
    //       Validators.min(3)
    //     ]);
    //   });

    // console.log();
  }
  fetchTransaction() {
    this.spinnerService.show();
    this.transactionService.getTransactionById(this.currentUser, this.currentRoute.snapshot.params['id'])
      .subscribe(res => {
        this.spinnerService.hide();
        this.transactionForm.controls['amount'].setValue(res.amount);
        this.transactionForm.controls['currency'].setValue(res.currency);
        this.transactionForm.controls['txn_date'].setValue(res.txn_date);
      }, err => {
        this.spinnerService.hide();
        this.toasterService.showToaster('Something Went Wrong');
      });
  }

  saveTransactionDetails(formRef) {
    this.transactionForm.value['txn_date'] = this.datePipe.transform(this.transactionForm.value['txn_date'], 'yyyy-MM-dd');
    if (this.currentPath === 'edit') {
      this.transactionForm.value['id'] = this.currentRoute.snapshot.params['id'];

      this.transactionService.updateTransaction(this.transactionForm.value).subscribe(res => {
        this.toasterService.showToaster('Updated Successfully');
        this.router.navigate(['transactions/list']);
      }, err => {
        this.toasterService.showToaster('Something Went Wrong');
      });
    } else if (this.currentPath === 'create') {
      this.transactionForm.value['user'] = this.currentUser;
      this.transactionService.createTransaction(this.transactionForm.value).subscribe(res => {
        this.toasterService.showToaster('Created Successfully');
        this.router.navigate(['transactions/list']);
      }, err => {
        this.toasterService.showToaster('Something Went Wrong');
      });
    }
    console.log(this.transactionForm.value);
  }

  canDeactivate() {
    const keys = Object.keys(this.transactionForm.value);
    const hasValue = keys.some(x => this.transactionForm.value[x]);
    return hasValue && !this.form.submitted ? confirm('Some form fields has values and form is not submitted. Continue?') : true;
  }

}
