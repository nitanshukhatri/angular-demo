import { Injectable } from '@angular/core';

@Injectable()
export class SpinnerService {
    private spinnerCache = new Set<any>();
    constructor() { }
    _register(spinner): void {
        this.spinnerCache.add(spinner);
    }

    show() {
        this.spinnerCache.forEach(spinner => {
            spinner.showSpinner = true;
        });
    }
    hide() {
        this.spinnerCache.forEach(spinner => {
            spinner.showSpinner = false;
        });
    }
}
