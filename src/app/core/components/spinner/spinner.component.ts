import { Component, Injector, Input, OnInit } from '@angular/core';
import { SpinnerService } from './spinner.service';

@Component({
  selector: 'app-spinner',
  template: `
    <div *ngIf="showSpinner" class="spinner">
      <img *ngIf="loadingImage" [src]="loadingImage" />
      <mat-spinner></mat-spinner>
    </div>
  `,
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent implements OnInit {
  spinnerService: SpinnerService;
  constructor(private injector: Injector) {
    this.spinnerService = injector.get(SpinnerService);
  }

  @Input() loadingImage: string;

  showSpinner = false;

  ngOnInit() {
    this.spinnerService._register(this);
  }

}
