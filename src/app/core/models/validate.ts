import { FormControl, AbstractControl } from '@angular/forms';

export class Validate {
    static isMin(val) {
        return function (c: AbstractControl) {
            if (c && c.parent) {
                // console.log(c);
                //  const control = c.parent.get(val);
                //  console.log(c.parent.get('amount').value.toString().length);

                if (c.parent.get('amount').value.toString().length < val) {
                    return { minChar: true };
                } else {
                    return;
                }
            }
        };
    }
}
