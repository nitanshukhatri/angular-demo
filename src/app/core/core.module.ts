import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ShowErrorsComponent } from './components/show-errors.component';
import { MatFormFieldModule, MatDialog, MatDialogModule } from '@angular/material';
import { ToasterService } from './services/toaster.service';
import { DialogService } from './services/dialog.service';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatDialogModule
  ],
  declarations: [
    ShowErrorsComponent
  ],
  providers: [
    ToasterService,
    DialogService
  ],
  exports: [
    ShowErrorsComponent
  ],
})
export class CoreModule { }
