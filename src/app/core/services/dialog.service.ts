import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../components/confirm-dialog/confirm-dialog.component';

@Injectable({ providedIn: 'root' })
export class DialogService {

    constructor(private dialog: MatDialog) {
    }

    openDialog(msg: string) {
        return this.dialog.open(ConfirmDialogComponent, {
            width: '390px',
            disableClose: true,
            panelClass: 'confirm-dialog-container',
            position: { top: '10px' },
            data: {
                message: msg
            }
        });
    }
}
