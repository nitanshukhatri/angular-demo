import { Injectable } from '@angular/core';


const keyTokenName = 'user';

@Injectable({ providedIn: 'root' })
export class BrowserDataService {
  constructor() { }
  getCurrentUser(): any {
    return JSON.parse(localStorage.getItem(keyTokenName));
  }

  setCurrentUser(user) {
    localStorage.setItem(keyTokenName, JSON.stringify(user));
  }

  removeCurrentUser() {
    localStorage.removeItem(keyTokenName);
  }


}
